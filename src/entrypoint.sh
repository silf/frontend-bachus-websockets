#!/usr/bin/env bash

cmd="$@"
sed s/"HTTP_PORT"/"${VIDEO_HTTP_PORT}"/ /etc/nginx/conf.d/default.conf.template | sed s/"VIDEO_IP:VIDEO_PORT"/"${VIDEO_IP}:${VIDEO_PORT}"/ > /etc/nginx/conf.d/default.conf 


#nginx -g "daemon off;"
#bash

exec $cmd
