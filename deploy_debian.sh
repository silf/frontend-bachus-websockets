#!/bin/bash

#run as root

set -e
#apt-get update
apt-get install nginx python-yaml python-jinja2

CAMS_CONF=cameras.yml
ENV_FILE=production.env

. ./$ENV_FILE

mkdir -p html
python ./camera_conf.py $CAMS_CONF

#Place config file in nginx etc tree
cp ${NGINX_SITE_CONF} /etc/nginx/sites-available/default 
ln -s -f /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default 

#Place test pages in nginx root location
mkdir -p $VIDEO_SERVER_ROOT
cp ./html/*.html $VIDEO_SERVER_ROOT

service nginx restart

#Clean
rm $NGINX_SITE_CONF
rm -r ./html
