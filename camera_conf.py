#!/usr/bin/python

import yaml, sys
from os import mkdir, path
from StringIO import StringIO
from jinja2 import Template
from os import environ as env

def print_usage(name):
    print('Usage: {} Camera_conf_yml'.format(name))
    exit(1)

def read_cameras_config(f_path):
    """
    Read cameras config (IP and etc) from yaml file

    Args:
        f_path: path to config yaml file
    Returns:
        dict: dictionary with cameras configuration like IP number
    """
    with open(f_path, 'r') as f:
        try:
            cam=yaml.load(f)
        except yaml.YAMLError as exc:
            print(exc)
            exit(1)
    return cam

#Write site (based on 't_path' template) to 'out_path'
def write_site(t_path, cam, env, descr , out_path):
    """
    Write nginx site or html page based on template

    Args:
        t_path: path to template file
        cam: dictionary with cameras configuration
        env: system ENVIRONMENT variables
        descr: string with short description of camera
               (used to generate html test page for camera)
        out_path: path to output file of nginx site or html page
    """
    with open (t_path, "r") as f:
        templ=f.read()
        template = Template(templ)
        out_str=template.render(cam=cam, env=env, descr=descr)
        with open(out_path, "w") as f_out:
             f_out.write(out_str)
         
def main(argv):
    if len(argv) != 2:
        print_usage(argv[0])
    if not path.isfile(argv[1]):
        print('No such file: {} !!!'.format(argv[1]))
        print_usage(argv[0])

    cam_config_file = argv[1]

    #Load cameras IP and etc into 'cam' variable
    cam = read_cameras_config(cam_config_file)

    #Write nginx cameras site
    write_site(env['NGINX_SITE_TEMPLATE'], cam, env, '', env['NGINX_SITE_CONF'])

    try:
        mkdir('html')
    except OSError as e:
        if e.errno != 17:
            raise
        pass
    #Write cameras test pages
    for c in cam:
        write_site(env['TEST_PAGE_TEMPLATE'], c, env, cam[c]['desc'], 'html/cam_{}.html'.format(c) )

    #Write index.html
    write_site(env['INDEX_TEMPLATE'], cam, env, '', 'html/index.html')


if __name__ == "__main__":
    main(sys.argv)
